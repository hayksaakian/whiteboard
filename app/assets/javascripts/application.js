// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require detect_timezone
//= require jquery.detect_timezone
//= require bootstrap
//= require highcharts
//= require highcharts/highcharts-more
//= require turbolinks
//= require best_in_place
//= require jquery.purr
//= require best_in_place.purr
//= require_tree .

$(document).ready(function() {
  /* Activating Best In Place */
  jQuery(".best_in_place").best_in_place();
  // var oTable = $('#progress_table').dataTable( {
 	// 	"sScrollX": "100%",
 	// 	"sScrollXInner": "150%",
 	// 	"bScrollCollapse": true
 	// } );
 	// new FixedColumns( oTable, {
 	// 	"iLeftColumns": 5,
		// "iLeftWidth": 350
 	// } );
});