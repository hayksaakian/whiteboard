class Task
  include Mongoid::Document
  include Mongoid::Timestamps
  has_and_belongs_to_many :users
  has_many :accomplishments
  field :description, type: String
  field :daily_limit, type: Integer, default: 1
  field :points, type: Float
  def max_points
  	return self.points * self.daily_limit
  end
  def possible_scores
    return (0..self.daily_limit).map{|x| x*self.points }
  end
  # validate that points assigned are not below 0 or above max
  def share_with(from_user, other_user)
    if from_user.tasks.include? self
      other_user.tasks.push(self)
      # may be pointless:
      other_user.save
    else
      errors.add(:users, 'You must have access to this task to share it')
    end
  end
end
