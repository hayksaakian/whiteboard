class Accomplishment
  include Mongoid::Document
  include Mongoid::Timestamps
  # not sure if I need this:
  include Mongoid::MultiParameterAttributes
  belongs_to :task
  field :score, type: Float
  field :completed_at, type: DateTime
  field :date, type: Date
  before_update :mark_complete
  def mark_complete
  	if self.score > 0
  		self.completed_at = Time.zone.now.to_datetime
  	end
  end
  def max_points
  	self.task.max_points
  end
  validates :score, format: { :with => /[-+]?(\.\d+|\d+(\.\d+)?)/, :message => 'has to match number regex' }, :on => :update
  # validates_numericality_of :score, :greater_than_or_equal_to => 0, :on => :update, :message => 'has to be a number'
  validate :score_is_number, :on => :update #, message: 'must be a number'
  validate :check_score, :on => :update
  # validates :score, format: { :with => /^\d+$/, :message => "must be a number"}

#   validate :check_score, :on => :update

  private 
  def score_is_number
    if !self.score.is_a? Numeric
      errors.add(:score, "Must be a number")
    end
  end
  def check_score 
    # puts "address: #{self.address.inspect}" 
    if (self.score == nil || self.task.possible_scores.include?(self.score) == false) && self.score.is_a?(Numeric)
      # puts "adding error" 
      # self.score = 'nil' if self.score == nil
      errors.add(:score, "#{self.score} must no more than "+self.max_points.to_s+'')
      # puts "errors now: #{errors}" 
    end 
  end 
end
