class AccomplishmentsController < ApplicationController
  before_filter :authenticate_user!#, :except => [:index, :new]
  load_and_authorize_resource
  def today
    @accomplishments = []
    # today = Date.today
    today = Time.now.in_time_zone.to_date
    current_user.tasks.each do |tsk|
      ac = tsk.accomplishments.find_or_create_by(:date => today)
      @accomplishments.push(ac)
    end
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @accomplishments }
    end
  end
  def index
    d = params[:d]
    num_days = (d && !d.blank?) ? (d.to_i) : 7
    puts params[:d]
    today = Time.now.in_time_zone.to_date
    # range = ((today-num_days)..today).to_a
    # TODO filter for active tasks
    # @tasks = current_user.tasks
    # @days = Hash.new
    # range.map{ |n| @days[n] => nil }
    # @tasks = Hash.new
    # range.each do |d|
    #   acs = []
    #   @tasks.each do |t|
    #     acs.push(t.accomplishments.where(:date => d))
    #   end
    #   @days[d] = acs
    # end

    @tasks = current_user.tasks if current_user
    @days = ((today-num_days)..today).to_a
    @days.reverse!
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: {:days => @days, :tasks => @tasks} }
    end    
  end
  def update
    @accomplishment = Accomplishment.find(params[:id])

    respond_to do |format|
      if @accomplishment.update_attributes(params[:accomplishment])
        format.html { redirect_to(today_path, :notice => 'Accomplishment was successfully updated.') }
        format.json { respond_with_bip(@accomplishment) }
      else
        format.html { render :action => "today" }
        format.json { respond_with_bip(@accomplishment) }
      end
    end
  end
end
