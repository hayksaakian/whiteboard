class ApplicationController < ActionController::Base
  protect_from_forgery
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
  before_filter :set_time_zone
	def set_time_zone
    #current user is a devise method see https://github.com/plataformatec/devise
    Time.zone = current_user.time_zone if current_user
	end
end
